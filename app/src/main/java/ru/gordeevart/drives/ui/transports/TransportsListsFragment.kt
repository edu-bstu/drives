package ru.gordeevart.drives.ui.transports

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ru.gordeevart.drives.R
import ru.gordeevart.drives.data.transports.TransportListItem

/**
 * A simple [Fragment] subclass.
 * Use the [TransportsListsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TransportsListsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_transports_lists, container, false)

        val transportLists = arrayListOf(
            TransportListItem("ТС в розыске", arrayListOf("A423KC31", "B635HT31", "T333CC31")),
            TransportListItem("Неплательщики", arrayListOf("O432KK31", "H540OT31")),
            TransportListItem("COVID", arrayListOf("O143OE31", "A548BM31"))
        )

        val recyclerSearchListView = root.findViewById<RecyclerView>(R.id.recyclerTransportListView)
        recyclerSearchListView.adapter = TransportListAdapter(transportLists)
        recyclerSearchListView.layoutManager = LinearLayoutManager(root.context)

        return root
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment TransportsListsFragment.
         */
        @JvmStatic
        fun newInstance() =
            TransportsListsFragment().apply {

            }
    }
}