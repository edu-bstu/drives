package ru.gordeevart.drives.ui.reports

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import okhttp3.OkHttpClient
import ru.gordeevart.drives.data.ServerConfig
import ru.gordeevart.drives.data.report.ReportDataSource
import ru.gordeevart.drives.data.report.ReportRepository

class ReportViewModelFactory : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ReportViewModel::class.java)) {
            return ReportViewModel(
                reportRepository = ReportRepository(
                    reportDataSource = ReportDataSource(ServerConfig.serverConfig, OkHttpClient())
                )
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}