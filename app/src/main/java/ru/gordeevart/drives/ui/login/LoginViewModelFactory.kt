package ru.gordeevart.drives.ui.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import okhttp3.OkHttpClient
import ru.gordeevart.drives.data.login.AuthService
import ru.gordeevart.drives.data.login.LoginDataSource
import ru.gordeevart.drives.data.login.LoginRepository
import ru.gordeevart.drives.data.ServerConfig.serverConfig

/**
 * ViewModel provider factory to instantiate LoginViewModel.
 * Required given LoginViewModel has a non-empty constructor
 */
class LoginViewModelFactory : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
            return LoginViewModel(
                loginRepository = LoginRepository(
                    dataSource = LoginDataSource(AuthService(serverConfig, OkHttpClient()))
                )
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}