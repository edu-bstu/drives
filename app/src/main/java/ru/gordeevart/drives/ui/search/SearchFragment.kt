package ru.gordeevart.drives.ui.search

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import ru.gordeevart.drives.R
import ru.gordeevart.drives.data.model.SearchList
import ru.gordeevart.drives.ui.Utils.getDateTime
import ru.gordeevart.drives.ui.main.PageViewModel
import java.time.LocalTime

/**
 * A placeholder fragment containing a simple view.
 */
class SearchFragment : Fragment() {

    private lateinit var searchViewModel: SearchViewModel
    private lateinit var pageViewModel: PageViewModel

    private val violations = arrayListOf(
        "1. Управление ТС с установленными с нарушением требований государственного стандарта " +
                "государственными регистрационными знаками (перевернутый гос.номер)",
        "2. Превышение установленной скорости движения ТС",
        "3. Выезд на железнодорожный переезд при закрытом или закрывающемся шлагбауме либо при " +
                "запрещающем сигнале светофора",
        "4. Остановка на железнодорожном переезде",
        "5. Стоянка на железнодорожном переезде",
        "6. Выезд на встречную полосу дороги на железнодорожном переезде"
    )

    private val posts = arrayListOf(
        "FP2655", "FP0259", "DS-2CD4026FWD/FP-AI20170909CCWR831539461", "DSAC024", "FP2075"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pageViewModel = ViewModelProvider(this).get(PageViewModel::class.java).apply {
            setIndex(arguments?.getInt(ARG_SECTION_NUMBER) ?: 1)
        }
        searchViewModel = ViewModelProvider(this, SearchViewModelFactory())
            .get(SearchViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_search, container, false)

        val searchBtn = root.findViewById<Button>(R.id.searchBtn)
        val dateFromPicker = root.findViewById<EditText>(R.id.dateFromInput)
        val dateToPicker = root.findViewById<EditText>(R.id.dateToInput)
        val vrpInput = root.findViewById<EditText>(R.id.vrpInput)
        val colorInput = root.findViewById<EditText>(R.id.colorInput)
        val modelInput = root.findViewById<EditText>(R.id.modelInput)
        val postAutoComplete = root.findViewById<AutoCompleteTextView>(R.id.postAutoComplete)
        val violationAutoComplete =
            root.findViewById<AutoCompleteTextView>(R.id.violationAutoComplete)

        postAutoComplete.threshold = 1
        violationAutoComplete.threshold = 1

        postAutoComplete.setAdapter(
            ArrayAdapter(
                context ?: throw IllegalStateException(),
                android.R.layout.simple_dropdown_item_1line, posts
            )
        )

        violationAutoComplete.setAdapter(
            ArrayAdapter(
                context ?: throw IllegalStateException(),
                android.R.layout.simple_dropdown_item_1line, violations
            )
        )

        searchBtn.setOnClickListener {

            searchViewModel.search(
                getDateTime(dateFromPicker)?.atTime(LocalTime.MIN),
                getDateTime(dateToPicker)?.atTime(LocalTime.MAX), vrpInput.text.toString(),
                colorInput.text.toString(), modelInput.text.toString(), null, null,
                null
            )
        }

        searchViewModel.searchResult.observe(viewLifecycleOwner, Observer {
            val searchResult = it ?: return@Observer

            if (searchResult.error != null) {
                showFailed(searchResult.error, context)
            }
            if (searchResult.success != null) {
                updateUiWithList(searchResult.success)
            }
        })

        return root
    }

    private fun showFailed(@StringRes errorString: Int, context: Context?) {
        Toast.makeText(context, errorString, Toast.LENGTH_SHORT).show()
    }

    private fun updateUiWithList(model: SearchList) {

        val intent = Intent(context, SearchListActivity::class.java).apply {
            val bundle = Bundle()
            bundle.putSerializable("searchList", model)
            putExtras(bundle)
        }

        startActivity(intent)
    }

    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance(sectionNumber: Int): SearchFragment {
            return SearchFragment().apply {
                arguments = Bundle().apply {

                }
            }
        }
    }
}