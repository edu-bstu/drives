package ru.gordeevart.drives.ui.search

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import ru.gordeevart.drives.R
import ru.gordeevart.drives.data.model.SearchItem
import ru.gordeevart.drives.ui.PhotoActivity

class SearchListAdapter(private val drives: List<SearchItem>)
    : RecyclerView.Adapter<SearchListAdapter.ViewHolder>() {

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textView: TextView

        init {
            // Define click listener for the ViewHolder's View.
            textView = view.findViewById(R.id.textView)
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.text_row_item, viewGroup, false)

        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        val drive = drives.get(position)
        viewHolder.textView.text = drive.getCardText()
        viewHolder.textView.setOnClickListener {

            val context = viewHolder.itemView.context
            val intent = Intent(context, PhotoActivity::class.java).apply {
                val bundle = Bundle()
                bundle.putString("id", drive.id)
                putExtras(bundle)
            }

            startActivity(context, intent, null)
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = drives.size
}