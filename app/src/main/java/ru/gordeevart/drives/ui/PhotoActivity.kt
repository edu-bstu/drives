package ru.gordeevart.drives.ui

import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.squareup.picasso.Picasso
import ru.gordeevart.drives.R


class PhotoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo)

        val driveId = intent.extras?.getString("id")

        val photoView = findViewById<ImageView>(R.id.photoView)

        Picasso
            .get()
//            .load(serverConfig.getUrl("/api/drives-photos/$driveId").toString())
            .load("https://storage1.censor.net/images/1/3/4/7/1347cca840c1de8af90d0dea190da994/640x544.jpg")
            .into(photoView)
    }
}