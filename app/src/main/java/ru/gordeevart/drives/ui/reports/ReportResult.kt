package ru.gordeevart.drives.ui.reports

import java.io.InputStream

data class ReportResult(
    val success: InputStream? = null,
    val error: Int? = null
)