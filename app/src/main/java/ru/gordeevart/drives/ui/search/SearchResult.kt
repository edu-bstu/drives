package ru.gordeevart.drives.ui.search

import ru.gordeevart.drives.data.model.SearchList

data class SearchResult(
    val success: SearchList? = null,
    val error: Int? = null
)
