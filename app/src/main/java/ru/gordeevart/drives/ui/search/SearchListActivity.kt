package ru.gordeevart.drives.ui.search

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ru.gordeevart.drives.R
import ru.gordeevart.drives.data.model.SearchList

class SearchListActivity : AppCompatActivity() {

    lateinit var searchList: SearchList

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_list)

        when (val argument = intent.extras?.getSerializable("searchList")) {
            is SearchList -> searchList = argument
        }

        val totalCountLabel = findViewById<TextView>(R.id.totalCountLabel)

        "Найдено ${searchList.totalCount} проездов".also { totalCountLabel.text = it }

        val recyclerSearchListView = findViewById<RecyclerView>(R.id.recyclerSearchListView)
        recyclerSearchListView.adapter = SearchListAdapter(searchList.drives)
        recyclerSearchListView.layoutManager = LinearLayoutManager(this)

    }
}