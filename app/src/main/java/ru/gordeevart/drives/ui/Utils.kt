package ru.gordeevart.drives.ui

import android.widget.EditText
import java.time.LocalDate

object Utils {

    fun getDateTime(datePicker: EditText): LocalDate? =
        if (datePicker.text.isBlank()) null else LocalDate.parse(datePicker.text)
}