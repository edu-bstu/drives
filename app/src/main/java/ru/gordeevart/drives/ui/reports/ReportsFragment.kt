package ru.gordeevart.drives.ui.reports

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.RadioGroup
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import ru.gordeevart.drives.R
import ru.gordeevart.drives.ui.Utils.getDateTime
import java.io.FileNotFoundException
import java.time.LocalDateTime
import java.time.LocalTime

/**
 * A simple [Fragment] subclass.
 * Use the [ReportsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ReportsFragment : Fragment() {

    private lateinit var reportViewModel: ReportViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        reportViewModel = ViewModelProvider(this, ReportViewModelFactory())
            .get(ReportViewModel::class.java)

        // Inflate the layout for this fragment
        val inflate = inflater.inflate(R.layout.fragment_reports, container, false)

        val reportBtn = inflate.findViewById<Button>(R.id.reportBtn)

        reportBtn?.setOnClickListener {

            val dateFromPicker = inflate.findViewById<EditText>(R.id.reportDateFromText)
            val dateToPicker = inflate.findViewById<EditText>(R.id.reportDateToText)
            val typeInput = inflate.findViewById<RadioGroup>(R.id.radioGroup)

            val type = when (typeInput.checkedRadioButtonId) {
                R.id.radioRangeButton -> "range"
                R.id.radioDistrictButton -> "district"
                R.id.radioDamageButton -> "damage"
                else -> {
                    throw IllegalAccessException("Выберите тип отчета")
                }
            }

            reportViewModel.report(
                getDateTime(dateFromPicker)?.atTime(LocalTime.MIN)
                    ?: throw IllegalArgumentException("Введите дату начала"),
                getDateTime(dateToPicker)?.atTime(LocalTime.MAX)
                    ?: throw IllegalArgumentException("Введите дату окончания"),
                type
            )
        }

        reportViewModel.reportResult.observe(viewLifecycleOwner, Observer {
            val reportResult = it ?: return@Observer

            if (reportResult.error != null) {
                showFailed(reportResult.error, context)
            }

            if (reportResult.success != null) {

                val fileName = "report-" + LocalDateTime.now().toString() + ".csv"

                reportResult.success.use { input ->

                    context?.openFileOutput(fileName, Context.MODE_PRIVATE).use { output ->

                        input.copyTo(output ?: throw FileNotFoundException())

                        Toast.makeText(
                            inflate.context,
                            "Отчет успешно скачан $fileName",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            }
        })

        return inflate
    }

    private fun showFailed(@StringRes errorString: Int, context: Context?) {
        Toast.makeText(context, errorString, Toast.LENGTH_SHORT).show()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment ReportsFragment.
         */
        @JvmStatic
        fun newInstance() =
            ReportsFragment().apply {

            }
    }
}