package ru.gordeevart.drives.ui.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import ru.gordeevart.drives.R
import ru.gordeevart.drives.data.Result
import ru.gordeevart.drives.data.search.SearchRepository
import java.time.LocalDateTime

class SearchViewModel(private val searchRepository: SearchRepository): ViewModel() {

    private val _searchResult = MutableLiveData<SearchResult>()
    val searchResult: LiveData<SearchResult> = _searchResult

    fun search(dateFrom: LocalDateTime?, dateTo: LocalDateTime?, vrp: String?,
              color: String?, model: String?, violation: Int?, postId: String?,
              listId: Int?) {

        viewModelScope.launch {

            val result =
                searchRepository.search(dateFrom, dateTo, vrp, color, model, violation, postId, listId)

            if (result is Result.Success) {
                _searchResult.value = SearchResult(success = result.data)
            } else {
                _searchResult.value = SearchResult(error = R.string.server_request_failed)
            }
        }
    }
}