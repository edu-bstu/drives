package ru.gordeevart.drives.ui.search

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import okhttp3.OkHttpClient
import ru.gordeevart.drives.data.search.SearchDataSource
import ru.gordeevart.drives.data.search.SearchRepository
import ru.gordeevart.drives.data.ServerConfig

class SearchViewModelFactory : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SearchViewModel::class.java)) {
            return SearchViewModel(
                searchRepository = SearchRepository(
                    dataSource = SearchDataSource(ServerConfig.serverConfig, OkHttpClient())
                )
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}