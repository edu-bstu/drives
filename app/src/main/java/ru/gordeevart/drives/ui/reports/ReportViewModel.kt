package ru.gordeevart.drives.ui.reports

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import ru.gordeevart.drives.R
import ru.gordeevart.drives.data.Result
import ru.gordeevart.drives.data.report.ReportRepository
import java.time.LocalDateTime

class ReportViewModel(private val reportRepository: ReportRepository): ViewModel() {

    private val _reportResult = MutableLiveData<ReportResult>()
    val reportResult: LiveData<ReportResult> = _reportResult

    fun report(dateFrom: LocalDateTime, dateTo: LocalDateTime, type: String) {

        viewModelScope.launch {

            val result = reportRepository.report(dateFrom, dateTo, type)

            if (result is Result.Success) {
                _reportResult.value = ReportResult(success = result.data)
            } else {
                _reportResult.value = ReportResult(error = R.string.server_request_failed)
            }
        }
    }
}