package ru.gordeevart.drives.ui.main

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import ru.gordeevart.drives.R
import ru.gordeevart.drives.ui.reports.ReportsFragment
import ru.gordeevart.drives.ui.search.SearchFragment
import ru.gordeevart.drives.ui.transports.TransportsListsFragment

private val TAB_ADMIN_TITLES = arrayOf(
    R.string.tab_search,
    R.string.tab_car_lists,
    R.string.tab_reports
)

private val TAB_TITLES = arrayOf(
    R.string.tab_search, R.string.tab_car_lists
)

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class SectionsPagerAdapter(private val context: Context, fm: FragmentManager, role: String) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val withReports = role == "operator"

    override fun getItem(position: Int): Fragment {

        if (!withReports) {
            return when (position) {
                0 -> SearchFragment.newInstance(0)
                else -> TransportsListsFragment.newInstance()
            }
        }

        return when (position) {
            0 -> SearchFragment.newInstance(0)
            1 -> TransportsListsFragment.newInstance()
            else -> ReportsFragment.newInstance()
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(
            if (withReports) TAB_ADMIN_TITLES[position] else TAB_TITLES[position]
        )
    }

    override fun getCount(): Int {
        return (if (withReports) TAB_ADMIN_TITLES else TAB_TITLES).size
    }
}