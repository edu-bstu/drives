package ru.gordeevart.drives.data.dto

data class AuthDto(val login: String, val password: String)