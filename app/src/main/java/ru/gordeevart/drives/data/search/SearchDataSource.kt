package ru.gordeevart.drives.data.search

import com.fasterxml.jackson.module.kotlin.readValue
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import ru.gordeevart.drives.data.JacksonMapper.jacksonMapper
import ru.gordeevart.drives.data.Result
import ru.gordeevart.drives.data.model.SearchItem
import ru.gordeevart.drives.data.model.SearchList
import ru.gordeevart.drives.data.model.ServerConfig
import java.io.IOException
import java.nio.charset.StandardCharsets
import java.time.LocalDateTime


class SearchDataSource(
    private val serverConfig: ServerConfig,
    private val client: OkHttpClient
) {

    val JSON: MediaType = "application/json; charset=utf-8".toMediaType()

    fun search(
        dateFrom: LocalDateTime?, dateTo: LocalDateTime?, vrp: String?, color: String?,
        model: String?, violation: Int?, postId: String?, listId: Int?
    ): Result<SearchList> = try {

        val url = buildUrl(dateFrom, dateTo, vrp, color, model, violation, postId, listId)
        val request = Request
            .Builder()
            .url(url)
            .header("Content-Type", JSON.toString())
            .get()
            .build()

        val call: Call = client.newCall(request)
        val response = call.execute()

        if (!response.isSuccessful) {
            throw Exception("Search failed, " + response.message)
        } else {

            val content = response.body?.bytes()?.let {
                String(it, StandardCharsets.UTF_8)
            }

            val searchList: List<SearchItem> = jacksonMapper.readValue(content ?: "")

            Result.Success(SearchList(searchList, getTotalCount(response, searchList)))
        }
    } catch (e: Throwable) {
        Result.Error(IOException("Ошибка запроса", e))
    }

    private fun getTotalCount(
        response: Response,
        searchList: List<SearchItem>
    ) = response.header("X-Total-Count")?.toLong() ?: searchList.size.toLong()

    private fun buildUrl(
        dateFrom: LocalDateTime?,
        dateTo: LocalDateTime?,
        vrp: String?,
        color: String?,
        model: String?,
        violation: Int?,
        postId: String?,
        listId: Int?
    ): HttpUrl {

        val urlBuilder = serverConfig.urlBuilder(serverConfig.searchPath)

        dateFrom?.also {
            urlBuilder.addQueryParameter("dateFrom", dateFrom.toString())
        }

        dateTo?.also {
            urlBuilder.addQueryParameter("dateTo", dateTo.toString())
        }

        vrp?.also {
            urlBuilder.addQueryParameter("vrp", vrp.toString())
        }

        color?.also {
            urlBuilder.addQueryParameter("color", color)
        }

        model?.also {
            urlBuilder.addQueryParameter("model", model)
        }

        violation?.also {
            urlBuilder.addQueryParameter("violation", violation.toString())
        }

        postId?.also {
            urlBuilder.addQueryParameter("postId", postId.toString())
        }

        listId?.also {
            urlBuilder.addQueryParameter("listId", listId.toString())
        }

        return urlBuilder.build()
    }
}