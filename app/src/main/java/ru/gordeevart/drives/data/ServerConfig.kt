package ru.gordeevart.drives.data

import ru.gordeevart.drives.data.model.ServerConfig

object ServerConfig {

    val serverConfig: ServerConfig = ServerConfig(
        "http","10.0.2.2", 8080, "/api/drives",
        "/api/auth/login", "/api/reports"
    )
}