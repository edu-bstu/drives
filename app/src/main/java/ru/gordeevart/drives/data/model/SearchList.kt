package ru.gordeevart.drives.data.model

import java.io.Serializable

data class SearchList(val drives: List<SearchItem>, val totalCount: Long) : Serializable
