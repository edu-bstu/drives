package ru.gordeevart.drives.data.model

import java.io.Serializable
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME

data class SearchItem(val id: String, val driveAt: LocalDateTime, val vrp: String,
                      val color: String, val model: String, val violation: String?,
                      val postId: String, val listId: Int?) : Serializable {

    fun getCardText(): String =
        "${driveAt.format(ISO_LOCAL_DATE_TIME)} ГРЗ $vrp $color $model.\nКамера фиксации $postId"
}