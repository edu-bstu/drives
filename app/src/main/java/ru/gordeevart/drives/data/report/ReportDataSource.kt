package ru.gordeevart.drives.data.report

import okhttp3.Call
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import ru.gordeevart.drives.data.Result
import ru.gordeevart.drives.data.model.ServerConfig
import java.io.IOException
import java.io.InputStream
import java.time.LocalDateTime

class ReportDataSource(private val serverConfig: ServerConfig,
                       private val client: OkHttpClient) {

    val CSV: MediaType = "text/csv".toMediaType()

    fun report(
        dateFrom: LocalDateTime, dateTo: LocalDateTime, type: String
    ): Result<InputStream> = try {

        val urlBuilder = serverConfig.urlBuilder(serverConfig.reportPath)

        urlBuilder.addQueryParameter("dateFrom", dateFrom.toString())
        urlBuilder.addQueryParameter("dateTo", dateTo.toString())
        urlBuilder.addQueryParameter("type", type)

        val url = urlBuilder.build()
        val request = Request
            .Builder()
            .url(url)
            .header("Content-Type", CSV.toString())
            .get()
            .build()

        val call: Call = client.newCall(request)
        val response = call.execute()

        if (!response.isSuccessful) {
            throw Exception("Search failed, " + response.message)
        } else {

            val resultStream = response.body?.byteStream()

            Result.Success(resultStream ?: throw IllegalStateException("Empty server response"))
        }
    } catch (e: Throwable) {
        Result.Error(IOException("Ошибка запроса", e))
    }
}