package ru.gordeevart.drives.data.model

data class AuthConfig(val authPath: String)