package ru.gordeevart.drives.data.login

import ru.gordeevart.drives.data.Result
import ru.gordeevart.drives.data.model.LoggedInUser
import java.io.IOException


/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
class LoginDataSource(private val authService: AuthService) {

    fun login(username: String, password: String): Result<LoggedInUser> = try {
        Result.Success(authService.authorize(username, password))
    } catch (e: Throwable) {
        Result.Error(IOException("Ошибка входа", e))
    }
}