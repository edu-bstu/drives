package ru.gordeevart.drives.data.search

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.gordeevart.drives.data.Result
import ru.gordeevart.drives.data.model.SearchList
import java.time.LocalDateTime

class SearchRepository(val dataSource: SearchDataSource) {

    suspend fun search(dateFrom: LocalDateTime?, dateTo: LocalDateTime?, vrp: String?,
                       color: String?, model: String?, violation: Int?, postId: String?,
                       listId: Int?): Result<SearchList> {

        return withContext(Dispatchers.IO) {

            dataSource.search(dateFrom, dateTo, vrp, color, model, violation, postId, listId)
        }
    }
}