package ru.gordeevart.drives.data.model

import okhttp3.HttpUrl

data class ServerConfig(
    val scheme: String, val host: String, val port: Int, val searchPath: String,
    val authPath: String, val reportPath: String
) {

    fun urlBuilder(path: String) =
        HttpUrl.Builder().scheme(scheme).port(port).host(host).encodedPath(path)

    fun getUrl(path: String) = urlBuilder(path).build()
}