package ru.gordeevart.drives.data.dto

data class AuthResponseDto(val name: String, val role: String)