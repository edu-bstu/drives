package ru.gordeevart.drives.data.model

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
data class LoggedInUser(val jwt: String, val name: String, val role: String)