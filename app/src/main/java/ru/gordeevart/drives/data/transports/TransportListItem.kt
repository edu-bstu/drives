package ru.gordeevart.drives.data.transports

data class TransportListItem(private val caption: String, private val vrpList: List<String>) {
    fun getCardText(): String = caption + "\nГРЗ: " + vrpList.joinToString("; ") + "\n"
}
