package ru.gordeevart.drives.data.login

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonTypeRef
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import ru.gordeevart.drives.data.dto.AuthDto
import ru.gordeevart.drives.data.dto.AuthResponseDto
import ru.gordeevart.drives.data.model.LoggedInUser
import ru.gordeevart.drives.data.model.ServerConfig

class AuthService(
    private val serverConfig: ServerConfig,
    private val client: OkHttpClient
) {

    val JSON: MediaType = "application/json; charset=utf-8".toMediaType()

    fun authorize(username: String, password: String): LoggedInUser {

        val authDto = AuthDto(username, password)
        val jsonBody = jacksonObjectMapper().writeValueAsString(authDto)

        val request = Request
            .Builder()
            .url(serverConfig.getUrl(serverConfig.authPath))
            .header("Content-Type", JSON.toString())
            .post(jsonBody.toRequestBody(JSON))
            .build()

        val call: Call = client.newCall(request)
        val response = call.execute()

        if (!response.isSuccessful) {
            throw Exception("Login failed, " + response.message)
        } else {

            val jwt = getJsonWebToken(response)
            val authResponseDto: AuthResponseDto =
                jacksonObjectMapper().readValue(response.body?.string(), jacksonTypeRef<AuthResponseDto>())

            return LoggedInUser(jwt, authResponseDto.name, authResponseDto.role)
        }
    }

    private fun getJsonWebToken(response: Response) = response
        .headers("Authorization")
        .first()
        .replaceFirst("Bearer\\s+", "")
}