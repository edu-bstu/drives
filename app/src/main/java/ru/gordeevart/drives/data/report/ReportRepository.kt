package ru.gordeevart.drives.data.report

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.gordeevart.drives.data.Result
import java.io.InputStream
import java.time.LocalDateTime

class ReportRepository(val reportDataSource: ReportDataSource) {

    suspend fun report(
        dateFrom: LocalDateTime, dateTo: LocalDateTime, type: String
    ): Result<InputStream> {

        return withContext(Dispatchers.IO) {

            reportDataSource.report(dateFrom, dateTo, type)
        }
    }
}